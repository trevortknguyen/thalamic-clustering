# thalamic-clustering

Open `otherClustering.R` in RStudio and run the code line-by-line.

# installing dependencies
You need `ggplot2` and `dplyr` and `Rtsne` to run the code.

You can install each using `install.packages("ggplot2")` for example.

# In the works
- tSNE visualization
- PCA-based clustering

# TODO list
- DBScan
- Silhouette analysis